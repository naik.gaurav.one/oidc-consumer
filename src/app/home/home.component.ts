import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  constructor(private oauthService: OAuthService) {
  }

  ngOnInit() {
    console.log('home');
  }

  public login() {
      this.oauthService.initImplicitFlow();
  }

  public logoff() {
      this.oauthService.logOut();
  }

  public claims() {
    console.log(this.oauthService.getIdentityClaims());
  }

  public idtoken() {
    console.log(this.oauthService.getIdToken());
  }

  public get name() {
      let claims: any = this.oauthService.getIdentityClaims();
      if (!claims) return null;
      return claims.given_name;
  }

}
