import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  
  requireHttps: false,

  // Url of the Identity Provider
  issuer: 'http://localhost:3000',

  // URL of the SPA to redirect the user to after login
  redirectUri: 'http://localhost:4200/index.html',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  clientId: '92f68e44-7935-4746-8b95-934935a9cc66',

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid',
  oidc: true,
  disableAtHashCheck: true
}